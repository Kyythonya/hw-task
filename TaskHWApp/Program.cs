﻿using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using unirest_net.http;

namespace TaskHWApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = GetResult("Asanali", "Dilnaz").Result;
            var compability = JsonConvert.DeserializeObject<Compatibility>(result);

            Console.WriteLine($"Love calculator \n{compability.FirstPartner} + {compability.SecondPartner} = {compability.Percentage}% \n{compability.Result}");

            Console.ReadLine();
        }

        static Task<string> GetResult(string firstPartnerName, string secondPartnerName)
        {
            HttpResponse<string> response = Unirest.get($"https://love-calculator.p.rapidapi.com/getPercentage?fname={firstPartnerName}&sname={secondPartnerName}")
               .header("X-RapidAPI-Key", "35797d2002msh3be02707d8e72e9p1e1fa2jsn48c2b146bb07")
               .asString();

            return Task.FromResult(response.Body);
        }
    }
}
